"""
Copyright (C) 2022  Fabio Mazza
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from collections import namedtuple
import sys
import time
import datetime
import argparse
from threading import Lock
import urllib.error
import http.client
from pathlib import Path
from warnings import warn
import requests
import gtt_updates
import io_lib, iomsg
import matolib
import logging
import logging.handlers

import signal

from concurrent.futures import ThreadPoolExecutor, wait

executor = ThreadPoolExecutor(3)

LOGGER= None

MONITOR_CONF = "monitoring.json"
BASE_NAME_OUT = "updates_{}.msgpack.zstd"
OUT_FOLDER="data"
TIME_SLEEP = 3
HOUR_CUT=(3,45)
MAX_UPDATES=130_000

HOURS_REMAKE_SESS = 0.6

PATTERNS_FUT = []
TRIPS_FUT = []
PATTERNS_LOCK = Lock()
TRIPS_LOCK = Lock()
N_TRIPS_SAVED = 0

format_date = lambda date : f"{date.year}{date.month:02d}{date.day:02d}{date.hour:02d}{date.minute:02d}"
format_date_day =  lambda date : f"{date.year}{date.month:02d}{date.day:02d}"
format_date_halfmonth =  lambda date : f"{date.year}{date.month:02d}00" if date.day < 15 else f"{date.year}{date.month:02d}15"
format_date_sec = lambda date : f"{date.year}{date.month:02d}{date.day:02d}{date.hour:02d}{date.minute:02d}{date.second:02d}"

format_date_pattern = format_date_halfmonth
get_patterns_fname = lambda dt: f"patterns_{format_date_pattern(dt)}.json.zstd"
get_pattern_fname_path = lambda outfold, dt: outfold/ get_patterns_fname(dt)

get_trips_fname = lambda dt: f"trips_{io_lib.format_date_twodays(dt)}.json.zstd"
get_trips_fname_path = lambda outfold,dt: outfold/ get_trips_fname(dt)

### EXIT WITH SYSTEMD
def handle_exit(sig, frame):
    raise SystemExit()

signal.signal(signal.SIGTERM, handle_exit)


def create_mparser():

    parser = argparse.ArgumentParser("Updater saver")

    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--no-data", action="store_true",dest="no_data")
    parser.add_argument("-j","--journal", dest="journal",action="store_true", help="Save log with systemd journal")
    parser.add_argument("-lf","--logfile", dest="logfile", default="", help="File to save log to")

    return parser

def loginfo(message):
    global LOGGER
    LOGGER.info(message)
def logdebug(message):
    global LOGGER
    if LOGGER is not None:
        LOGGER.debug(message)
    else:
        print(message)

def download_patternInfo(patterncode):
    global PATTERNS_DOWN
    try:
        pattern = matolib.get_pattern_info(patterncode)
        pattern["timestamp"] = int(time.time())
        code = pattern["code"]
        with PATTERNS_LOCK: 
            ## use lock
            if code not in PATTERNS_DOWN:
                PATTERNS_DOWN[code] = [pattern]
            else:
                found = False
                #h1 = hash(pattern["stops"])
                for p in PATTERNS_DOWN[code]:
                    if p["patternGeometry"]["points"] == pattern["patternGeometry"]["points"]:
                        found = True
                        p["timestamp"] = pattern["timestamp"]
                        break
                if not found:
                    print(f"Current pattern {code} not found, appending")
                    PATTERNS_DOWN[code].append(pattern)

            #PATTERNS_DOWN[code] = pattern
        
    except Exception as e:
        logdebug(f"Cannot download pattern {patterncode}, ex: {e}", file=sys.stderr)

def last_pattern_timestamp(patts):
    t=-3
    for p in patts:
        if p["timestamp"] > t:
            t = p["timestamp"]
    return t

def download_tripinfo(gtfsname, lineid):
    global DOWNLOADED_TRIPS, TRIPS_DOWN, PATTERNS_DOWN
    
    gtfsid=f"gtt:{gtfsname}"
    if gtfsid=="gtt:NoneU":
        # in some cases the trip is a text 'None'
        return
    if gtfsid in DOWNLOADED_TRIPS:
        ## already downloaded
        return
    try:
        trip_d = matolib.get_trip_info(gtfsid)
        md = datetime.datetime.now()
        tripelm = dict(gtfsId=trip_d["gtfsId"], serviceId=trip_d["serviceId"], headsign=trip_d["tripHeadsign"],
                               routeId=trip_d["route"]["gtfsId"], patternCode=trip_d["pattern"]["code"],
                               patternDate=format_date_pattern(md))
        #print(f"Download trip {gtfsid} info, route {lineid}")
        with TRIPS_LOCK:
            TRIPS_DOWN.append(tripelm)
            DOWNLOADED_TRIPS.add(gtfsid)

        patCode = tripelm["patternCode"]
        ts = int(time.time())
        with PATTERNS_LOCK:
            if(patCode not in PATTERNS_DOWN) or (
                last_pattern_timestamp(PATTERNS_DOWN[patCode])-ts > (2)*3600): ## 8 hours have already passed
                PATTERNS_FUT.append(
                    executor.submit(download_patternInfo, patCode)
                )

    except Exception as e:
        ### nothing work
        #print(f"Download info for trip {gtfsid},  gtfsid: {gtfsid}"")
        logdebug(f"Failed to download data for trip {gtfsid}, route {lineid} error: {e}")

def save_patterns_done(patterns_file, reset_saving=False):
    global PATTERNS_FUT, PATTERNS_DOWN, OUT_FOLDER
    res = wait(PATTERNS_FUT)
    with PATTERNS_LOCK:
        PATTERNS_FUT = list(filter(lambda x: not x.done(),PATTERNS_FUT))
        t = time.time()
        io_lib.save_json_zstd(patterns_file,PATTERNS_DOWN, level=5)
        loginfo(f"Saved the patterns in {time.time()-t:.3f} s")

        if reset_saving:
             loginfo("Reset patterns")
             #PATTERNS_FNAME = get_pattern_fname_path(Path(OUT_FOLDER), datetime.datetime.now())
             PATTERNS_DOWN = {}


def save_trips_need(trips_file, reset_trips=False):
    global TRIPS_FUT, TRIPS_DOWN, N_TRIPS_SAVED, DOWNLOADED_TRIPS
    res = wait(TRIPS_FUT)
    with TRIPS_LOCK:
        TRIPS_FUT = list(filter(lambda x: not x.done(),TRIPS_FUT))
        if len(TRIPS_DOWN) > N_TRIPS_SAVED:
            ## save patterns
            t = time.time()
            io_lib.save_json_zstd(trips_file,TRIPS_DOWN, level=6)
            loginfo(f"Saved the trips in {time.time()-t:.3f} s")
            N_TRIPS_SAVED = len(TRIPS_DOWN)

        if reset_trips:
            TRIPS_DOWN = []
            DOWNLOADED_TRIPS = set()
            N_TRIPS_SAVED = 0


#def get_string_name_day()



class Update:
    """
    Basic class to hold the updates data
    """
    def __init__(self, up_data):
        news = up_data["vehicle"]
        tripdat = news["trip"]
        self.timest = news["timestamp"]
        self.veh_num = news["vehicle"]["label"]
        self.route =  tripdat["route_id"]
        self.trip_id = tripdat["trip_id"]
        #self.start_date 

        self.data = {
            "timestamp": self.timest,
            "veh": self.veh_num
        }
        for k in tripdat:
            self.data[k] = tripdat[k]
        for k in news["position"]:
            self.data[k] = news["position"][k]


    def __hash__(self):
        return hash((self.timest, self.veh_num, self.route, self.trip_id))
    
    def __eq__(self, other):
        return self.timest == other.timest and self.veh_num == other.veh_num and self.route == other.route

    def __str__(self):
        return f"ts: {self.timest}, veh: {self.veh_num}, route: {self.route}"

    def get_service_day(self):
        try:
            return self.data["start_date"]
        except:
            logdebug("Update has no start_date")
            return "00000000"

Result = namedtuple("Result", ["data","error"])

def get_parse_updates(session=None, ntries=40):
    gotit = False
    count=0
    while (not gotit and count<ntries):
        try:
            data = gtt_updates.get_updates(session=session)
            gotit = True
        except urllib.error.HTTPError as e:
            logdebug("Got "+e.msg+" , Retrying")
        except http.client.RemoteDisconnected as e:
            logdebug("Remote disconnected, Retrying")
        except Exception as ex:
            logdebug(f"Generic error: {ex}, retrying")
        finally:
            count+=1

    if not gotit:
        warn(f"Cannot get an update after {ntries} trials")
        logdebug(f"Cannot get an update after {ntries} trials")
        return Result(tuple(),"TRIALOUT")

    keys = data.keys()
    if len(keys)>2 or "header" not in keys or "entity" not in keys:
        logdebug(keys)
    
    if "entity" not in keys:
        logdebug("NO PAYLOAD")
        return Result(tuple(), "EMPTY")

    return Result((Update(d) for d in data["entity"]), None)

def get_cut_date(next_day=False):
    now = datetime.datetime.now()
    if next_day:
        now += datetime.timedelta(days=1.)
    defin = datetime.datetime(now.year, now.month, now.day , *HOUR_CUT)
    return defin

def process_updates(ups_set):
    gen = sorted(ups_set, key=lambda x: x.timest)
    return  [x.data for x in gen]

def save_updates_file(increm_list, outname):
    #print("Saving...", end="")
    t = time.time()
    #gen = sorted(fin_set, key=lambda x: x.timest)
    
    #io_lib.save_json_gzip(outname, [x.data for x in gen])
    #io_lib.save_json_zstd(outname, increm_list)
    iomsg.save_msgpack_zstd(outname, increm_list, level=5)
    #[x.data for x in gen])
    tsave = time.time()-t
    loginfo(f"Saving updates took {tsave:4.3f} s")

def setup_logging(args):
    global LOGGER
    LOGGER = logging.getLogger("gtt_gtfs_updates")
    LOGGER.setLevel(logging.DEBUG)

    fmtr = logging.Formatter(fmt='%(asctime)s -- %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
    if args.journal:
        from systemd.journal import JournalHandler
        handler = JournalHandler(SYSLOG_IDENTIFIER="gttGtfsUpdatesSaver")
        handler.setLevel(logging.INFO)
    else:
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(fmtr)
    
    LOGGER.addHandler(handler)
    if args.logfile != "":
        h2 = logging.handlers.RotatingFileHandler(args.logfile,maxBytes=10*1024*1024,backupCount=100)
        h2.setLevel(logging.DEBUG)
        h2.setFormatter(fmtr)
        LOGGER.addHandler(h2)

        return h2
    else:
        return None


def make_filename(outfold, kind="updates"):
    this_date = datetime.datetime.today()
    if kind =="updates":
        outname = outfold / Path(BASE_NAME_OUT.format(format_date(this_date)))
    elif kind == "log_no_data":
        outname = outfold / Path(f"log_nodata_{format_date(this_date)}.json.zstd")
    else:
        raise ValueError(f"kind {kind} not recognised")
    return outname


def main(argv):
    global PATTERNS_DOWN, DOWNLOADED_TRIPS, TRIPS_DOWN, TRIPS_FNAME, PATTERNS_FNAME

    parser=create_mparser()

    args = parser.parse_args(argv[1:])
    debug_run = True if args.debug else False

    possible_fhandler=setup_logging(args)
    try:
        conf_monitor = io_lib.read_json(MONITOR_CONF)
        UP_PINGER = io_lib.LimitedPinger(conf_monitor["url_ping"],4*60)
        UP_PINGER.ping_if_needed()
        loginfo("Started pinger for service")
        if "url_gtt_gtfs_ping" in conf_monitor:
            GTFS_UP_PINGER = io_lib.LimitedPinger(conf_monitor["url_gtt_gtfs_ping"], 2*60)
            loginfo("Started pinger for GTFS data")
        else:
            GTFS_UP_PINGER = None
    except Exception as e:
        print("Cannot load monitoring configuration, error:",e)
        UP_PINGER=io_lib.LimitedPinger(None,10000)
        GTFS_UP_PINGER = None

    no_data = args.no_data
    if no_data:
        loginfo("Will not download additional data on trips")

    m_session = requests.Session()
    starttime = int(time.time())
    tsess = int(starttime)
    ts_cut = get_cut_date().timestamp()
    if ts_cut < starttime:
        date_next = get_cut_date(next_day=True)
        loginfo(f"Updating saving date to {date_next}")
        ts_cut = date_next.timestamp()
    outfold = Path(OUT_FOLDER)
    timenow = datetime.datetime.now()
    PATTERNS_FNAME = get_pattern_fname_path(outfold, timenow)
    TRIPS_FNAME = get_trips_fname_path(outfold, timenow)

    if(PATTERNS_FNAME.exists()):
        PATTERNS_DOWN = io_lib.read_json_zstd(PATTERNS_FNAME)
    else:
        loginfo("No patterns file")
        PATTERNS_DOWN = {}

    if(TRIPS_FNAME.exists()):
        TRIPS_DOWN = io_lib.read_json_zstd(TRIPS_FNAME)
        DOWNLOADED_TRIPS = set(t["gtfsId"] for t in TRIPS_DOWN)
        loginfo(f"Loaded {len(DOWNLOADED_TRIPS)} trips")

    else:
        loginfo("No trips file")
        TRIPS_DOWN = []
        DOWNLOADED_TRIPS = set()

    firstres=get_parse_updates(m_session)

    fin_updates = set(firstres.data)
    UPDATES_OUT = process_updates(fin_updates)
    LOG_NO_DATA = []
    count = 1
    
    if not outfold.exists():
        outfold.mkdir(parents=True)
    
    #this_date = datetime.datetime.today()
    FILE_SAVE = make_filename(outfold)
    LOG_NO_DATA_FILE = make_filename(outfold, "log_no_data")
    mtimestamp = int(time.time())
    mdate = datetime.datetime.today()

        
    ### begin
    try:
        diff_lup = -1
        last_up_t = int(time.time())
        isstartworking = False
        while True:
        
            loginfo(f"Num updates: {len(fin_updates):6d} trips:{len(TRIPS_DOWN):4d}, pat:{len(PATTERNS_DOWN):4d}")

            time.sleep(TIME_SLEEP)
            gotnewdata = False
            
            MAXC  = 15
            cempty = 0
            while gotnewdata is False:
                UP_PINGER.ping_if_needed()
                try:
                    newres = get_parse_updates(m_session)
                    if newres.error == None:
                        gotnewdata = True
                        diff_lup  = time.time()-last_up_t
                        if diff_lup > 30:
                            io_lib.update_set_outage_date(LOG_NO_DATA, last_up_t, int(time.time()))
                        last_up_t = int(time.time())
                        if GTFS_UP_PINGER != None:
                            GTFS_UP_PINGER.ping_if_needed()
                    elif newres.error == "TRIALOUT":
                        loginfo("Remake session and retry")
                        m_session = requests.Session()
                        tsess = time.time()
                    else:
                        cempty+=1

                        if newres.error!= "EMPTY": 
                            logdebug("ERROR: "+newres.error)
                        else: 
                            time.sleep(2)
                        diff_lup=int(time.time()-last_up_t)
                        if diff_lup > 30:
                            io_lib.update_set_outage_date(LOG_NO_DATA, last_up_t, int(time.time()))
                            loginfo("last update: "+str(datetime.timedelta(seconds=diff_lup))+" ago")
                        if cempty >= MAXC:
                            ## exit loop
                            gotnewdata = True

                except (requests.exceptions.ConnectionError, requests.exceptions.RequestException):
                    loginfo("Error, Remake session")
                    m_session = requests.Session()
                    tsess = time.time()
            ## find ones that have to be added
            ups_add = set(newres.data).difference(fin_updates)
            fin_updates = fin_updates.union(ups_add)
            ## add missing to list
            UPDATES_OUT.extend(process_updates(ups_add))
            ### add updates tripId
            if not no_data:
                for up in ups_add:
                    TRIPS_FUT.append(
                        executor.submit(download_tripinfo, up.trip_id, up.route)
                    )

            mtimestamp = int(time.time())
            mdate = datetime.datetime.today()
            count += 1
            rotate_file_save = mtimestamp > ts_cut or len(fin_updates) > MAX_UPDATES
            if count % 10 == 0 or rotate_file_save:
                ## save the updates
                save_updates_file(UPDATES_OUT, FILE_SAVE)
                io_lib.save_json_zstd(LOG_NO_DATA_FILE, LOG_NO_DATA, level=5)
            if count % 15 == 0:
                
                #save_patterns_done(PATTERNS_FNAME)
                #save_trips_need(TRIPS_FNAME)
                datet = datetime.datetime.now()
                ##PATTERNS
                reset_patterns = get_pattern_fname_path(outfold,datet).name != PATTERNS_FNAME.name
                executor.submit(save_patterns_done, PATTERNS_FNAME, reset_patterns)
                if reset_patterns:
                    ## update patterns name
                    PATTERNS_FNAME = get_pattern_fname_path(outfold, datet)
                    loginfo(f"Changing patterns file: {PATTERNS_FNAME}")

                ##TRIPS
                reset_trips = get_trips_fname_path(outfold,datet).name != TRIPS_FNAME.name
                executor.submit(save_trips_need, TRIPS_FNAME, reset_trips)
                if reset_trips:
                    ## update patterns name
                    TRIPS_FNAME = get_trips_fname_path(outfold, datet)
                    loginfo(f"Changing trips file: {TRIPS_FNAME}")

            if rotate_file_save:
                ### UPDATE HOUR TO SAVE
                if mtimestamp > ts_cut:
                    assert get_cut_date().timestamp() < mtimestamp
                    date_next = get_cut_date(next_day=True)
                    loginfo(f"Updating saving date to {date_next}")
                    ts_cut = date_next.timestamp()
                
                FILE_SAVE = make_filename(outfold) #outfold / Path(BASE_NAME_OUT.format(mtimestamp))
                LOG_NO_DATA_FILE = make_filename(outfold, "log_no_data")
                fin_updates = set()
                UPDATES_OUT = []
                LOG_NO_DATA = []
            if time.time() - tsess > HOURS_REMAKE_SESS*3600:
                loginfo("Remaking session")
                m_session = requests.Session()
                tsess = time.time()
    except (KeyboardInterrupt, SystemExit):
        loginfo("Have to shutdown, saving files")
        save_updates_file(UPDATES_OUT, FILE_SAVE)
        io_lib.save_json_zstd(LOG_NO_DATA_FILE, LOG_NO_DATA, level=5)
        save_trips_need(TRIPS_FNAME)
        save_patterns_done(PATTERNS_FNAME)
        loginfo("All done")
    #print([hash(l) for l in v])


if __name__ == '__main__':
    main(sys.argv)
