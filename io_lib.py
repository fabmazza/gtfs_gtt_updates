"""
Copyright (C) 2022  Fabio Mazza
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import json
import gzip
import io
import zstandard
import requests
import time

def read_json(f, mode="r"):
    with open(f,mode) as io:
        data = json.load(io)
    return data

def ping_url(URL):
    requests.get(URL)
 
def save_json_gzip(fpath, obj):
    with gzip.open(fpath, "wt") as f:
        json.dump(obj, f, indent=2)

def save_json_zstd(fpath, obj, level=10):
    cctx = zstandard.ZstdCompressor(threads=-1,level=level)
    with open(fpath, "wb") as f:
        with cctx.stream_writer(f) as compressor:
            wr = io.TextIOWrapper(compressor, encoding='utf-8')
            json.dump(obj, wr)
            wr.flush()
            wr.close()

def read_json_zstd(f):
    decomp = zstandard.ZstdDecompressor()
    with open(f,mode="rb") as f:
        with decomp.stream_reader(f) as st:
            dat=json.load(st)
    return dat

def format_date_twodays(date):
    day_p = date.day if date.day % 2 == 1 else date.day-1
    return f"{date.year}{date.month:02d}{day_p:02d}"

def update_set_outage_date(log_nodata, last_tup, current_time):
    append = False
    if (len(log_nodata) ==0):
        append = True
    elif log_nodata[-1]["time_from"] == last_tup:
        log_nodata[-1]["time_to"] = current_time
    else:
        append = True
    
    if append:
        log_nodata.append({"time_from": last_tup, "time_to":current_time})

class LimitedPinger:
    def __init__(self, url_ping, min_time) -> None:
        self.URL = url_ping
        self.lastping = -1
        self.min_time = min_time
    
    def ping_if_needed(self, force=False):
        if self.URL == None or self.URL == "":
            ## do nothing
            return
        ping = force
        mtime = int(time.time())
        if not ping:
            ping = (mtime-self.lastping) > self.min_time
        if ping:
            #print("Ping URL")
            requests.get(self.URL)
            self.lastping = mtime